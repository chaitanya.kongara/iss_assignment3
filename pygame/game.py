#!/usr/bin/python
# -*- coding: utf-8 -*-
import pygame

from config import *

import time

from pygame.locals import K_UP, K_DOWN, K_LEFT, K_RIGHT, K_ESCAPE, \
    KEYDOWN, QUIT, K_RETURN

pygame.init()

screen = pygame.display.set_mode((screen_width, screen_height))
screen.fill((0, 150, 150))

obs = pygame.Surface((screen_width, 50))
obs.fill(white)
rect = obs.get_rect()

screen.blit(obs, (0, 0))
screen.blit(obs, (0, (screen_height-50)/6))
screen.blit(obs, (0, (screen_height-50)/3))
screen.blit(obs, (0, (screen_height-50)/2))
screen.blit(obs, (0, ((screen_height-50)*2)/3))
screen.blit(obs, (0, ((screen_height-50)*5)/6))
screen.blit(obs, (0, ((screen_height-50))))

# icon = pygame.image.load('mario.png')
# pygame.display.set_caption("my_game")
# pygame.display.set_icon(icon)

playerimg = pygame.transform.scale(pygame.image.load('mario.png'), (50,
                                   50))
obsimg = pygame.transform.scale(pygame.image.load('pirate.png'), (50,
                                50))
mvimg = pygame.transform.scale(pygame.image.load('pirateship.png'),
                               (75, 50))
player2img = pygame.transform.scale(pygame.image.load('luigi.png'),
                                    (70, 60))
coinimg = pygame.transform.scale(pygame.image.load('coin.gif'), (50,
                                 50))

# pygame.transfer.scale(playerimg,(10,10))

var = 0
flag = 0
counter = 0
counter1 = 0
counter2 = 0
counter3 = 0
counter4 = 0


def player():
    screen.fill((0, 150, 150))
    obs = pygame.Surface((800, 50))
    obs.fill((181, 101, 29))
    rect = obs.get_rect()
    screen.blit(obs, (0, 0))
    screen.blit(obs, (0, (screen_height-50)/6))
    screen.blit(obs, (0, (screen_height-50)/3))
    screen.blit(obs, (0, (screen_height-50)/2))
    screen.blit(obs, (0, ((screen_height-50)*2)/3))
    screen.blit(obs, (0, ((screen_height-50)*5)/6))
    screen.blit(obs, (0, ((screen_height-50))))

    if counter == 0:
        screen.blit(coinimg, (coinX, coinY))
    if counter1 == 0:
        screen.blit(coinimg, (coin2X, coin2Y))
    if counter2 == 0:
        screen.blit(coinimg, (coin3X, coin3Y))
    if counter3 == 0:
        screen.blit(coinimg, (coin4X, coin4Y))
    if flag == 0:
        screen.blit(playerimg, (playerX, playerY))
    elif flag == 1:
        screen.blit(player2img, (player2X, player2Y))

    screen.blit(obsimg, (obs1X, obs1Y))
    screen.blit(obsimg, (obs2X, obs2Y))
    screen.blit(obsimg, (obs3X, obs3Y))
    screen.blit(obsimg, (obs4X, obs4Y))
    screen.blit(obsimg, (obs5X, obs5Y))
    screen.blit(obsimg, (obs6X, obs6Y))
    screen.blit(obsimg, (obs7X, obs7Y))
    screen.blit(obsimg, (obs8X, obs8Y))
    screen.blit(obsimg, (obs9X, obs9Y))
    screen.blit(obsimg, (obs10X, obs10Y))
    screen.blit(mvimg, (mv1X, mv1Y))
    screen.blit(mvimg, (mv2X, mv2Y))
    screen.blit(mvimg, (mv7X, mv7Y))
    screen.blit(mvimg, (mv3X, mv3Y))
    screen.blit(mvimg, (mv4X, mv4Y))
    screen.blit(mvimg, (mv5X, mv5Y))
    screen.blit(mvimg, (mv6X, mv6Y))


def message_display(text):
    global flag
    font = pygame.font.Font('freesansbold.ttf', 20)
    screen.fill((0, 0, 0))
    screen.blit(font.render(text, True, green), (150, 300))
    pygame.display.update()
    var = True
    while var:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    var = False
    player()


def text_objects(text, font):
    textSurface = font.render(text, True, (0, 255, 0))
    return (textSurface, textSurface.get_rect())


# def message_display(text):
# ....global flag
# ....largeText = pygame.font.Font('freesansbold.ttf',50)
# ....TextSurf, TextRect = text_objects(text,largeText)
# ....TextRect.center = (800/2,650/2)
# ....screen.fill((0,0,0))
# ....screen.blit(TextSurf,TextRect)

# ....pygame.display.update()
# ....time.sleep(0.5)
# ....player()

def message_display2(text):
    global flag
    largeText = pygame.font.Font('freesansbold.ttf', 30)
    (TextSurf, TextRect) = text_objects(text, largeText)
    TextRect = (150, 300)
    screen.fill((0, 0, 0))
    screen.blit(TextSurf, TextRect)

    pygame.display.update()
    player()


# def player1():
# ....screen.fill((0,150,150))
# ....obs = pygame.Surface((800,50))
# ....obs.fill((181,101,29))
# ....rect = obs.get_rect()
# ....screen.blit(obs , (0,0))
# ....screen.blit(obs , (0,100))
# ....screen.blit(obs , (0,200))
# ....screen.blit(obs , (0,300))
# ....screen.blit(obs , (0,400))
# ....screen.blit(obs , (0,500))
# ....screen.blit(obs , (0,600))
# ....screen.blit(playerimg,(375,600))
# ....screen.blit(obsimg,(obs1X,obs1Y))
# ....screen.blit(obsimg,(obs2X,obs2Y))
# ....screen.blit(obsimg,(obs3X,obs3Y))
# ....screen.blit(obsimg,(obs4X,obs4Y))
# ....screen.blit(obsimg,(obs5X,obs5Y))
# ....screen.blit(obsimg,(obs6X,obs6Y))
# ....screen.blit(obsimg,(obs7X,obs7Y))
# ....screen.blit(obsimg,(obs8X,obs8Y))
# ....screen.blit(obsimg,(obs9X,obs9Y))
# ....screen.blit(obsimg,(obs10X,obs10Y))
# ....screen.blit(mvimg,(mv1X,mv1Y))
# ....screen.blit(mvimg,(mv2X,mv2Y))
# ....screen.blit(mvimg,(mv7X,mv7Y))
# ....screen.blit(mvimg,(mv3X,mv3Y))
# ....screen.blit(mvimg,(mv4X,mv4Y))
# ....screen.blit(mvimg,(mv5X,mv5Y))
# ....screen.blit(mvimg,(mv6X,mv6Y))

# def player2():
# ....screen.fill((0,150,150))
# ....obs = pygame.Surface((800,50))
# ....obs.fill((181,101,29))
# ....rect = obs.get_rect()
# ....screen.blit(obs , (0,0))
# ....screen.blit(obs , (0,100))
# ....screen.blit(obs , (0,200))
# ....screen.blit(obs , (0,300))
# ....screen.blit(obs , (0,400))
# ....screen.blit(obs , (0,500))
# ....screen.blit(obs , (0,600))
# ....screen.blit(player2img,(450,0))
# ....screen.blit(obsimg,(obs1X,obs1Y))
# ....screen.blit(obsimg,(obs2X,obs2Y))
# ....screen.blit(obsimg,(obs3X,obs3Y))
# ....screen.blit(obsimg,(obs4X,obs4Y))
# ....screen.blit(obsimg,(obs5X,obs5Y))
# ....screen.blit(obsimg,(obs6X,obs6Y))
# ....screen.blit(obsimg,(obs7X,obs7Y))
# ....screen.blit(obsimg,(obs8X,obs8Y))
# ....screen.blit(obsimg,(obs9X,obs9Y))
# ....screen.blit(obsimg,(obs10X,obs10Y))
# ....screen.blit(mvimg,(mv1X,mv1Y))
# ....screen.blit(mvimg,(mv2X,mv2Y))
# ....screen.blit(mvimg,(mv7X,mv7Y))
# ....screen.blit(mvimg,(mv3X,mv3Y))
# ....screen.blit(mvimg,(mv4X,mv4Y))
# ....screen.blit(mvimg,(mv5X,mv5Y))
# ....screen.blit(mvimg,(mv6X,mv6Y))

def crash():
    global starttime
    global counter
    global counter1
    global counter2
    global counter3
    global flag
    global var
    if flag == 0:
        message_display('got caught (press enter to start game of player2)'
                        )
    elif flag == 1:
        if var == 1:
            message_display('got caught (press enter to start next round)'
                            )
        elif var == 3:
            message_display('got caught (press enter to see game results)'
                            )
    flag = 1 - flag
    starttime = time.time()
    counter = 0
    counter1 = 0
    counter2 = 0
    counter3 = 0
    var += 1


def game_intro():
    intro = True
    while intro:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    return
            if event.type == QUIT:
                pygame.quit()
                quit()
        font = pygame.font.SysFont('comicsansms', 50)
        value1 = \
            font.render('-> For every 2 sec there will be decrement',
                        True, (0, 255, 0))
        value2 = \
            font.render('  of 1 point from score of that particular player.',
                        True, (0, 255, 0))
        value3 = font.render('->  press enter to view instruction page.',
                             True, (0, 255, 0))
        value4 = \
            font.render('-> There is a chance of getting negative points,',
                        True, (0, 255, 0))
        value6 = font.render("    So Slow And Study isn't worthy here.",
                             True, (0, 255, 0))
        value7 = \
            font.render('-> collecting a coin leads to increment of ',
                        True, (0, 255, 0))
        value8 = font.render('   10 points.', True, (0, 255, 0))
        value5 = font.render('made by Chaitanya', True, (0, 255, 0))
        screen.fill((0, 0, 0))
        screen.blit(value1, (20, 30))
        screen.blit(value2, (20, 90))
        screen.blit(value4, (20, 150))
        screen.blit(value6, (20, 220))
        screen.blit(value7, (20, 300))
        screen.blit(value8, (20, 380))
        screen.blit(value3, (20, 460))
        screen.blit(value5, (220, 560))
        pygame.display.update()


def instructions():
    info = True
    while info:
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_RETURN:
                    return
            if event.type == QUIT:
                pygame.quit()
                quit()
        font = pygame.font.SysFont('comicsansms', 50)
        valuem = font.render('INSTRUCTIONS', True, (0, 255, 0))
        font = pygame.font.SysFont('freesansbold.ttf', 30)
        value1 = \
            font.render('1. Players are represented with Mario and Luigi.',
                        True, (0, 255, 0))
        font = pygame.font.SysFont('freesansbold.ttf', 30)
        value2 = \
            font.render('2. player needs to go to other end without getting ',
                        True, (0, 255, 0))
        value3 = font.render('   caught to pirates .', True, (0, 255,
                             0))
        value4 = font.render('3. buttons for Player1:', True, (0, 255,
                             0))
        value5 = font.render('       UP button to move up ', True, (0,
                             255, 0))
        value6 = font.render('       DOWN button to move down ', True,
                             (0, 255, 0))
        value7 = font.render('       LEFT Key to move left ', True, (0,
                             255, 0))
        value8 = font.render('       RIGHT Key to move Right ', True,
                             (0, 255, 0))
        value9 = font.render('4. buttons for Player2: ', True, (0, 255,
                             0))
        value10 = font.render('      DOWN button to move Up', True, (0,
                              255, 0))
        value11 = font.render('      UP button to move Down ', True,
                              (0, 255, 0))
        value12 = font.render('      LEFT Key to move right ', True,
                              (0, 255, 0))
        value13 = font.render('      RIGHT Key to move left', True, (0,
                              255, 0))
        new1 = \
            font.render("5. The danger zone consists of pirates",
                        True, (0, 255, 0))
        new2 = font.render(' them if you enter that zone.', True, (0, 255, 0))
        new3 = \
            font.render('6. Ships that are moving consists of pirates.',
                        True, (0, 255, 0))
        new5 = \
            font.render('7. After both the players complete their game twice,',
                        True, (0, 255, 0))
        new6 = \
            font.render('   the one with high score is declared as winner.',
                        True, (0, 255, 0))
        new4 = font.render('   Press Enter to start the game.', True,
                           (0, 255, 0))
        screen.fill((0, 0, 0))
        screen.blit(valuem, (250, 20))
        screen.blit(value1, (10, 80))
        screen.blit(value2, (10, 110))
        screen.blit(value3, (10, 140))
        screen.blit(value4, (10, 170))
        screen.blit(value5, (10, 200))
        screen.blit(value6, (10, 230))
        screen.blit(value7, (10, 260))
        screen.blit(value8, (10, 290))
        screen.blit(value9, (10, 320))
        screen.blit(value10, (10, 350))
        screen.blit(value11, (10, 380))
        screen.blit(value12, (10, 410))
        screen.blit(value13, (10, 440))
        screen.blit(new1, (10, 470))
        screen.blit(new2, (10, 500))
        screen.blit(new3, (10, 530))
        screen.blit(new4, (250, 620))
        screen.blit(new5, (10, 560))
        screen.blit(new6, (10, 590))
        pygame.display.update()


def mod(a):
    if a >= 0:
        return a
    elif 0 > a:
        return a * -1


font = pygame.font.Font('freesansbold.ttf', 15)


def show_score1(x, y):
    value1 = font.render('score_player1 :' + str(int(score_player1)),
                         True, (0, 0, 0))
    screen.blit(value1, (x, y))


def show_score2(x, y):
    value2 = font.render('score_player2 :' + str(int(score_player2)),
                         True, (0, 0, 0))
    screen.blit(value2, (x, y))


game_intro()
screen.fill(black)
pygame.display.update()
instructions()
player()

# starttime = some_time_library.somemodule()

starttime = time.time()
run = True
while run and var < 4:
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            quit()

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_LEFT:
                if flag == 0:
                    playerX -= 25
                elif flag == 1:
                    player2X += 25

            if event.key == pygame.K_RIGHT:
                if flag == 0:
                    playerX += 25
                elif flag == 1:
                    player2X -= 25
            if event.key == pygame.K_DOWN:
                if flag == 0:
                    playerY += 25
                elif flag == 1:
                    player2Y -= 25

            if event.key == pygame.K_UP:
                if flag == 0:
                    playerY -= 25
                elif flag == 1:
                    player2Y += 25

            # if event.key == pygame.K_RETURN:
            # ....if flag == 0:

                # crash()
                # playerY=600
                # playerX=375
        if playerY <= 0:
            playerY = 0
        if playerY >= screen_height - 50:
            playerY = screen_height - 50
        if playerX <= 0:
            playerX = 0
        if playerX >= screen_width - 50:
            playerX = screen_width - 50

        if player2Y <= 0:
            player2Y = 0
        if player2Y >= screen_height - 50:
            player2Y = screen_height - 50
        if player2X <= 0:
            player2X = 0
        if player2X >= screen_width - 50:
            player2X = screen_width - 50

    if mod(playerX - coinX) <= 25 and mod(playerY - coinY) <= 25:
        if counter == 0:
            counter = 1
            score_player1 += 10
        show_score1(textX, textY)
        player()
    if mod(playerX - coin2X) <= 25 and mod(playerY - coin2Y) <= 25:
        if counter1 == 0:
            counter1 = 1
            score_player1 += 10
        show_score1(textX, textY)
        player()
    if mod(playerX - coin3X) <= 25 and mod(playerY - coin3Y) <= 25:
        if counter2 == 0:
            counter2 = 1
            score_player1 += 10
        show_score1(textX, textY)
        player()
    if mod(playerX - coin4X) <= 25 and mod(playerY - coin4Y) <= 25:
        if counter3 == 0:
            counter3 = 1
            score_player1 += 10
        show_score1(textX, textY)
        player()
    if mod(player2X - coinX) <= 25 and mod(player2Y - coinY) <= 25:
        if counter == 0:
            counter = 1
            score_player2 += 10
        show_score2(textX + 600, textY)
        player()
    if mod(player2X - coin2X) <= 25 and mod(player2Y - coin2Y) <= 25:
        if counter1 == 0:
            counter1 = 1
            score_player2 += 10
        show_score2(textX, textY)
        player()
    if mod(player2X - coin3X) <= 25 and mod(player2Y - coin3Y) <= 25:
        if counter2 == 0:
            counter2 = 1
            score_player2 += 10
        show_score2(textX, textY)
        player()
    if mod(player2X - coin4X) <= 25 and mod(player2Y - coin4Y) <= 25:
        if counter3 == 0:
            counter3 = 1
            score_player2 += 10
        show_score2(textX, textY)
        player()
    if mod(playerX - obs10X) <= 25 and mod(playerY - obs10Y) <= 25:
        crash()
        counter = 0
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        min_var = 600
    if mod(playerX - obs9X) <= 25 and mod(playerY - obs9Y) <= 25:
        crash()
        counter = 0
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        min_var = 600
    if mod(playerX - obs8X) <= 25 and mod(playerY - obs8Y) <= 25:
        crash()
        counter = 0
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        min_var = 600
    if mod(playerX - obs7X) <= 25 and mod(playerY - obs7Y) <= 25:
        crash()
        counter = 0
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        min_var = 600
    if mod(playerX - obs6X) <= 25 and mod(playerY - obs6Y) <= 25:
        crash()
        counter = 0
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        min_var = 600
    if mod(playerX - obs5X) <= 25 and mod(playerY - obs5Y) <= 25:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        min_var = 600
    if mod(playerX - obs4X) <= 25 and mod(playerY - obs4Y) <= 25:
        crash()
        counter = 0
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        min_var = 600
    if mod(playerX - obs3X) <= 25 and mod(playerY - obs3Y) <= 25:
        crash()
        counter = 0
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        min_var = 600
    if mod(playerX - obs2X) <= 25 and mod(playerY - obs2Y) <= 25:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        min_var = 600
    if mod(playerX - obs1X) <= 25 and mod(playerY - obs1Y) <= 25:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        min_var = 600

    if mod(player2X - obs10X) <= 25 and mod(player2Y - obs10Y) <= 25:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        max_var = 0

    if mod(player2X - obs9X) <= 25 and mod(player2Y - obs9Y) <= 25:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        max_var = 0
    if mod(player2X - obs8X) <= 25 and mod(player2Y - obs8Y) <= 25:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        max_var = 0
    if mod(player2X - obs7X) <= 25 and mod(player2Y - obs7Y) <= 25:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        max_var = 0
    if mod(player2X - obs6X) <= 25 and mod(player2Y - obs6Y) <= 25:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        max_var = 0
    if mod(player2X - obs5X) <= 25 and mod(player2Y - obs5Y) <= 25:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        max_var = 0
    if mod(player2X - obs4X) <= 25 and mod(player2Y - obs4Y) <= 25:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        max_var = 0
    if mod(player2X - obs3X) <= 25 and mod(player2Y - obs3Y) <= 25:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        max_var = 0
    if mod(player2X - obs2X) <= 25 and mod(player2Y - obs2Y) <= 25:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        max_var = 0
    if mod(player2X - obs1X) <= 25 and mod(player2Y - obs1Y) <= 25:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        max_var = 0
    if mod(playerX - mv1X) < 50 and mod(playerY - mv1Y) < 50:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        min_var = screen_width - 50
    if mod(playerX - mv2X) < 50 and mod(playerY - mv2Y) < 50:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        min_var = screen_width - 50
    if mod(playerX - mv3X) < 50 and mod(playerY - mv3Y) < 50:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        min_var = screen_width - 50
    if mod(playerX - mv4X) < 50 and mod(playerY - mv4Y) < 50:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        min_var = screen_width - 50
    if mod(playerX - mv5X) < 50 and mod(playerY - mv5Y) < 50:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        min_var = screen_width - 50
    if mod(playerX - mv6X) < 50 and mod(playerY - mv6Y) < 50:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        min_var = screen_width - 50
    if mod(playerX - mv7X) < 50 and mod(playerY - mv7Y) < 50:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        min_var = screen_width - 50
    if mod(player2X - mv1X) < 50 and mod(player2Y - mv1Y) < 50:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        max_var = 0
    if mod(player2X - mv2X) < 50 and mod(player2Y - mv2Y) < 50:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        max_var = 0
    if mod(player2X - mv3X) < 50 and mod(player2Y - mv3Y) < 50:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        max_var = 0
    if mod(player2X - mv4X) < 50 and mod(player2Y - mv4Y) < 50:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        max_var = 0
    if mod(player2X - mv5X) < 50 and mod(player2Y - mv5Y) < 50:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        max_var = 0
    if mod(player2X - mv6X) < 50 and mod(player2Y - mv6Y) < 50:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        max_var = 0
    if mod(player2X - mv7X) < 50 and mod(player2Y - mv7Y) < 50:
        crash()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = int(screen_width*0.46875)
        max_var = 0

    if min_var > playerY:
        min_var = playerY
        if min_var / 10 % 10 == 5:
            score_player1 += 5
        elif min_var / 10 % 10 == 0:
            score_player1 += 10

    if max_var < player2Y:
        max_var = player2Y
        if max_var / 10 % 10 == 5:
            score_player2 += 5
        elif max_var / 10 % 10 == 0:
            score_player2 += 10

        # if mod(playerX-mv4X) <= 125 and mod(playerY-mv4Y) <= 125 :
        # ....crash()
        # ....playerY=600
        # ....playerX=375
        # print(playerX)
        # print(mv1X)

    if var < 2:
        mv1X = (mv1X + 0.5) % 800
        mv2X = (mv2X + 1) % 800
        mv3X = (mv3X + 1.5) % 800
        mv4X = (mv4X + 0.6) % 800
        mv5X = (mv5X + 0.4) % 800
        mv6X = (mv6X + 0.7) % 800
        mv7X = (mv7X + 0.4) % 800
    elif var >= 2 and var < 4:
        mv1X = (mv1X + 3) % 800
        mv2X = (mv2X + 5) % 800
        mv3X = (mv3X + 2) % 800
        mv4X = (mv4X + 1) % 800
        mv5X = (mv5X + 1.3) % 800
        mv6X = (mv6X + 1) % 800
        mv7X = (mv7X + 1.3) % 800
    if flag == 0:
        score_player1 -= (time.time() - starttime) / 2
    elif flag == 1:
        score_player2 -= (time.time() - starttime) / 2
    starttime = time.time()

    player()
    show_score1(textX, textY)
    show_score2(textX + 600, textY)
    pygame.display.update()
    if flag == 0 and playerY == 0:
        flag = 1
        counter = 0
        counter1 = 0
        counter2 = 0
        counter3 = 0
        starttime = time.time()
        player2X = int(screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = (screen_width*0.46875)
        var += 1
        player()
    if flag == 1 and player2Y == screen_height - 50:
        flag = 0
        counter = 0
        counter1 = 0
        counter2 = 0
        counter3 = 0
        starttime = time.time()
        player2X = (screen_width*0.66677)
        player2Y = 0
        playerY = screen_height - 50
        playerX = (screen_width*0.46875)
        player()
        var += 1
    pygame.display.update()

font = pygame.font.Font('freesansbold.ttf', 50)
font1 = pygame.font.Font('freesansbold.ttf', 75)
end = font1.render('GAMEOVER', True, green)
end1 = font.render('Player1 won the game ', True, green)
end2 = font.render('Player2 won the game ', True, green)
end3 = font.render("Player1's score is " + str(int(score_player1)),
                   True, green)
end4 = font.render("Player2's score is " + str(int(score_player2)),
                   True, green)
screen.fill(black)
screen.blit(end, (100, 50))
if score_player2 < score_player1:
    screen.blit(end1, (50, 150))
elif score_player1 < score_player2:
    screen.blit(end2, (50, 150))
screen.blit(end3, (20, 350))
screen.blit(end4, (20, 450))
pygame.display.update()
while run:
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                run = False
pygame.quit()
quit()
