import pygame

screen_width = 800
screen_height = 650

green = (0,255,0)
black = (0,0,0)
white = (255,255,255)
coinX = 700
coinY = 500

coin2X = 250
coin2Y = 300

coin3X = 400
coin3Y = 100

coin4X = 50
coin4Y = 500

playerX = 375
playerY = 600

player2X = 425
player2Y = 0

obs1Y = 0
obs1X = 375

obs2Y = 500
obs2X = 635

obs3Y = 100
obs3X = 750

obs4Y = 200
obs4X = 525

obs5Y = 400
obs5X = 400

obs6Y = 500
obs6X = 125

obs7Y = 300
obs7X = 350

obs8Y = 100
obs8X = 150

obs9Y = 200
obs9X = 200

obs10Y = 600
obs10X = 300

mv1X = 450
mv1Y = 50

mv2Y = 150
mv2X = 700

mv3Y = 350
mv3X = 450

mv4Y = 250
mv4X = 550

mv5Y = 450
mv5X = 100

mv6Y = 550
mv6X = 150

mv7Y = 450
mv7X = 600

score_player1 = 0
score_player2 = 0

textX = 10
textY = 10

min_var = 600
max_var = 0